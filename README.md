# HD BLU Life XL Upgrades

# About
I get a lot of questions about how to compile Android and how to make custom kernels, so I thought I would put together a video series with everything I know. (Don't worry, that will not take long!)

# Who is this course for?
This course is for those who are able to flash custom recoveries, like TRWP or CWM, and who can root their phones or flash custom roms. Now that you can do those things, you are ready to start building your very own custom roms and kernels!

# What do we cover?
In this series, we are attempting to build a device tree from scratch at Lollipop and upgrade it (hopefully) all the way to Pie. We'll see how we do and how far we get!

